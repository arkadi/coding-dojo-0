import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class DiamondTest {

	private Diamond diamond;

	@Before
	public void setUp() {
		diamond = new Diamond();
	}

	private void p(String str) { System.out.println(str+"\n");}

	@Test
	public void test1() {
		p(diamond.createDiamond(1));
		assertEquals("+", diamond.createDiamond(1));
	}
	
	@Test
	public void test2() {
		p(diamond.createDiamond(2));
		assertEquals(" +\n" +
                     "+ +\n" +
                     " +", diamond.createDiamond(2));
	}
	
	@Test
	public void test3() {
		p(diamond.createDiamond(3));
		assertEquals("  +\n" +
                     " + +\n" +
                     "+   +\n" +
                     " + +\n" +
                     "  +", diamond.createDiamond(3));
	}
	
	@Test
	public void test4() {
		p(diamond.createDiamond(4));
		assertEquals("   +\n" +
                     "  + +\n" +
                     " +   +\n" +
                     "+     +\n" +
                     " +   +\n" +
                     "  + +\n" +
                     "   +", diamond.createDiamond(4));
	}

}
