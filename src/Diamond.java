
public class Diamond {
	public String createDiamond(int size) {
		if (size == 1) {
			return "+";
		}
		StringBuilder sb = new StringBuilder();
		createHead(sb, size);
		sb.append("\n");
		for (int i = 0; i < size - 2; i++) {
			createMid(sb, size, i);
		}
		for (int i = size - 2; i >= 0; i--) {
			createMid(sb, size, i);
		}
		createHead(sb, size);
		return sb.toString();
	}
	
	public void createMid(StringBuilder sb, int size, int i) {
		addSpaces(sb, size - i - 2);
		sb.append('+');
		addSpaces(sb, i * 2 + 1);
		sb.append("+\n");
	}

	private void createHead(StringBuilder sb, int size) {
		addSpaces(sb, size - 1);
		sb.append("+");
	}
	
	private void addSpaces(StringBuilder sb, int count) {
		for(int i = 0; i < count; i++) {
			sb.append(' ');
		}
	}

}
